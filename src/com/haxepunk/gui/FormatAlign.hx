package com.haxepunk.gui;

#if (flash || html5)
typedef FormatAlign = flash.text.TextFormatAlign;
#else
typedef FormatAlign = String;
#end