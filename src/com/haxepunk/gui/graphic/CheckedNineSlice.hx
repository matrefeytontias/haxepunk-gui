package com.haxepunk.gui.graphic;

#if (flash || html5)
	typedef CheckedNineSlice = com.haxepunk.gui.graphic.flash.CheckedNineSlice;
#else
	typedef CheckedNineSlice = com.haxepunk.gui.graphic.native.CheckedNineSlice;
#end
