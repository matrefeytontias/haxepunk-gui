package com.haxepunk.gui.graphic;

#if (flash || html5)
	typedef NineSlice = com.haxepunk.gui.graphic.flash.NineSlice;
#else
	typedef NineSlice = com.haxepunk.gui.graphic.native.NineSlice;
#end
